#include "Data.h"
#include <math.h>
#include <time.h>
#include <stdio.h>
#define printout(r) {printf("%07d:%15e %15e %15e %15e %15e %15e %15e %15e\n",i,r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7]);}  

#define FLOAT double

inline FLOAT raise(float a){
  return pow((FLOAT)a,8.125);
}

inline FLOAT hmean(const FLOAT *B){
  FLOAT sum = 0.0;
  for(int i=0;i<DATA_DIM;i++) 
    sum += 1/B[i];
  return 8/sum;
}

inline void raiseall(const float *A, FLOAT *B){
  for(int i=0;i<DATA_DIM;i++)
    B[i] = raise(A[i]);
}

FLOAT compute(const Data& data){
  // IMPLEMENT ME!
  const int n = data.size();
  FLOAT sum=0.0;
  FILE* f = fopen("hval.dat","w");
  for (int i=0;i<n;i++){
    FLOAT h;
    FLOAT B[DATA_DIM];
    const float *A = data.value(i);//(int)random(0,data.size()));
    raiseall(A,B);
    h = hmean(B);
    fprintf(f,"%e\n",h);
    sum += h;
    //printout(A);
    //printout(B);
    //printf("Harmonic mean: %15e  Sum: %15e\n",h,sum);
    }
  fclose(f);
  printf("Result for %d : %e \n",n,sum/n);
  return sum/n;
}



