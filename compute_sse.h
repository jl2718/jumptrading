#include "Data.h"
#include <immintrin.h>
//#include "avxintrin_emu.h"

//#define debug

#ifdef debug
#include <stdio.h>

#define printout(a) {_mm_store_ps(r,a);printf("%07d:%15e %15e %15e %15e\n",i,r[0],r[1],r[2],r[3]);}  
#define printall {printout(a);printout(b);printout(c);printout(d);printout(e);printout(f);printout(g);printf("\n");}
#endif

float compute(const Data& data)
{
  // IMPLEMENT ME!
  float x[4];
  double r=0;
  #pragma unroll
  for (int i=0;i<data.size();i++){
    __m128 a,b,c,d,e,f,g;
    // load first 4
    a = _mm_load_ps(data.value(i));
    // b = a^-8
    b = _mm_mul_ps(a,a);
    b = _mm_mul_ps(b,b);
    b = _mm_mul_ps(b,b);
    b = _mm_rcp_ps(b);
    // c ~ a^(-1/8)
    c = _mm_rsqrt_ps(a);
    c = _mm_rsqrt_ps(c);
    c = _mm_rsqrt_ps(c);
    // d ~ a^(-65/8)  (debug only)
    d = _mm_mul_ps(b,c);
    // e ~ 1/SUM{a^(-65/8)}   
    e = _mm_dp_ps(b,c,0xff); 
    // f ~ half sum of inverse
    f = e;
    g = _mm_setzero_ps();
    #ifdef debug
    printall;    
    #endif

    // load next 4
    a = _mm_load_ps(data.value(i)+4);
    // b = a^-8
    b = _mm_mul_ps(a,a);
    b = _mm_mul_ps(b,b);
    b = _mm_mul_ps(b,b);
    b = _mm_rcp_ps(b);
    // c ~ a^(-1/8)
    c = _mm_rsqrt_ps(a);
    c = _mm_rsqrt_ps(c);
    c = _mm_rsqrt_ps(c);
    // d ~ a^(-65/8)  (debug only)
    #ifdef debug
    d = _mm_mul_ps(b,c);
    #endif
    // e ~ 1/SUM{a^(-65/8)}   
    e = _mm_dp_ps(b,c,0xff); 
    // f ~ full sum of inverse
    f = _mm_add_ps(f,e);

    // g ~ harmonic mean
    g = _mm_rcp_ps(f);
    // x ~ running sum
    _mm_store1_ps(x,g);
    r = r+(double)x[0]; 
    #ifdef debug
    printall;
    #endif
        #ifdef debug
    printout(x);printf("\n");
    #endif
    }
    return r*DATA_DIM/data.size();
}

