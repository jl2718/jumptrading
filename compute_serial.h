#include "Data.h"
#include <math.h>
#include <stdio.h>
#define printout(r) {printf("%07d:%15e %15e %15e %15e %15e %15e %15e %15e\n",i,r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7]);}  

#define FLOAT float

inline FLOAT raise(float a){
  return pow((FLOAT)a,8.125);
}

inline FLOAT hmean(const FLOAT *B){
  FLOAT sum = 0.0;
  for(int i=0;i<DATA_DIM;i++) 
    sum += 1/B[i];
  return 8/sum;
}

inline void raiseall(const float *A, FLOAT *B){
  for(int i=0;i<DATA_DIM;i++)
    B[i] = raise(A[i]);
}

float compute(const Data& data){
  // IMPLEMENT ME!
  FLOAT sum=0.0;
  for (int i=0;i<data.size();i++){
    FLOAT h;
    FLOAT B[DATA_DIM];
    const float *A = data.value(i);
    raiseall(A,B);
    h = hmean(B);
    sum += h;
    //printout(A);
    //printout(B);
    //printf("Harmonic mean: %15e  Sum: %15e\n",h,sum);
    }
  return sum/data.size();
}

