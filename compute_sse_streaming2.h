#include "Data.h"
#include <immintrin.h>
//#include "avxintrin_emu.h"

//#define debug

#ifdef debug
#include <stdio.h>

#define printout(a) {_mm_store_ps(x,a);printf("%07d:%15e %15e %15e %15e\n",i,x[0],x[1],x[2],x[3]);}  
#define printall {printout(a1);printout(b1);printout(c1);printout(d1);printout(h);printf("\n");}
#endif

float compute(const Data& data)
{
  // IMPLEMENT ME!
  float x[4];
  double r=0;
  #ifdef debug
  const int n = 10;
  #else
  const int n = data.size();
  #endif
  #pragma unroll
  for (int i=0;i<n;i++){
    __m128 a,b,c,d1,d2;
    // first 4
    a = _mm_load_ps(data.value(i));
    b = _mm_mul_ps(a,a);
    c = _mm_rsqrt_ps(a);
    b = _mm_mul_ps(b,b);
    c = _mm_rsqrt_ps(c);
    b = _mm_mul_ps(b,b);
    c = _mm_rsqrt_ps(c);
    b = _mm_rcp_ps(b);
    d1 = _mm_dp_ps(b,c,0xff);
    // next 4
    a = _mm_load_ps(data.value(i)+4);
    b = _mm_mul_ps(a,a);
    c = _mm_rsqrt_ps(a);
    b = _mm_mul_ps(b,b);
    c = _mm_rsqrt_ps(c);
    b = _mm_mul_ps(b,b);
    c = _mm_rsqrt_ps(c);
    b = _mm_rcp_ps(b);
    d2 = _mm_dp_ps(b,c,0xff);
    // h ~ harmonic mean
    __m128 h;
    h = _mm_add_ps(d1,d2);
    h = _mm_rcp_ps(h);
    // x ~ running sum
    _mm_store1_ps(x,h);
    r = r+(double)x[0]; 
    #ifdef debug
    printall;
    printf("%15e %15e\n\n",x[0],r);
    #endif
    }
    return r*DATA_DIM/data.size();
}

