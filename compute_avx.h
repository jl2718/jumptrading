#include "Data.h"
//#include <immintrin.h>
#include "avxintrin_emu.h"
#include <stdio.h>

float compute(const Data& data)
{
  // IMPLEMENT ME!
  float r[8];
  __m256 x = _mm256_setzero_ps();
  #pragma unroll
  for (int i=0;i<data.size();i++){
    __m256 a,b,c,d,e;
    
    a = _mm256_load_ps(data.value(i));
    // b = a^-8
    b = _mm256_mul_ps(a,a);
    b = _mm256_mul_ps(b,b);
    b = _mm256_mul_ps(b,b);
    b = _mm256_rcp_ps(b);
    // c ~ a^(-1/8)
    c = _mm256_rsqrt_ps(a);
    c = _mm256_rsqrt_ps(c);
    c = _mm256_rsqrt_ps(c);
    // d ~ a^(-65/8)  (debug only)
    //d = _mm256_mul_ps(b,c);
    // e ~ 1/SUM{a^(-65/8)}   
    e = _mm256_dp_ps(b,c,0xff); 
    e = _mm256_rcp_ps(e);
    // x ~ running sum
    x = _mm256_add_ps(x,e);    
    
    
    {
      _mm256_store_ps(r,a);printf("%07d:%e\n",i,r[0]);  
      _mm256_store_ps(r,b);printf("%07d:%e\n",i,r[0]); 
      _mm256_store_ps(r,c);printf("%07d:%e\n",i,r[0]); 
      _mm256_store_ps(r,d);printf("%07d:%e\n",i,r[0]); 
      _mm256_store_ps(r,e);printf("%07d:%e\n",i,r[0]);
      _mm256_store_ps(r,x);printf("%07d:%e\n",i,r[0]); 
    }
  }
  _mm256_store_ps(r,x);
  return r[0]*DATA_DIM/data.size();
}

